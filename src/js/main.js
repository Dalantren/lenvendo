$(document).ready(function() {

    let $body = $('body');

    //Делаем проверку инпутов после ввода чего-либо
    $body.on('change', '.input input', function () {

        let errors = validateInput($(this));

        if (typeof errors !== 'undefined' && errors.length) {
            $(this).closest('form').find('.error-text').text(errors[0].msg)
        } else {
            $(this).closest('form').find('.error-text').text('');
        }
    });

    //Запрос на удаление закладки
    $body.on('click', '.js-delete-bookmark', function() {
        if (!$(this).data('ajax') || !$(this).data('id')) {
            return;
        }

        let pass = $(this).data('hasPass') === 'y' ? prompt('Введите пароль для удаления', '') : '';

        $.ajax({
            data: $(this).data('ajax') + `&action=delete&pass=${pass}&id=` + $(this).data('id'),
            dataType: 'json',
            success: (resp) => {
                if (resp.status) {
                    if (!(/^bookmarks\/?$/).test(window.location.pathname)) {
                        window.location.href = '/bookmarks/';
                    } else {
                        // Обработка удаления на странице списка закладок, например
                    }
                } else if (resp.msg) {
                    alert(resp.msg);
                }
            }
        })
    })
});

function validateInput(el) {
    let errors = [];

    if (el.prop('type') === 'url' && el.val().length > 0 && !(/^http(s)?:\/\/[^ "]+\.[a-zA-Zа-яА-Я]+(\/)?(.)*?$/.test(el.val()))) {
        errors.push({msg : 'Введите валидный url!'});
    }

    if (el.prop('type') === 'password' && el.val().length >= 1 && (el.val().length < 4 || el.val().length > 20)) {
        errors.push({msg : 'Длина пароля должна быть от 4 до 20 символов'});
    }


    if (el.prop('type') === 'password' && el.val().length) {

        let anotherPassword = el.prop('name') === 'password' ? 'password_verify' : 'password';

        if (el.val() !== el.closest('form').find(`input[name="${anotherPassword}"]`).val()) {
            errors.push({msg : 'Пароли не совпадают'});
        } else {
            el
                .closest('form')
                .find(`input[name="${anotherPassword}"]`)
                .closest('.input')
                .removeClass('input--err')
                .addClass('input--ok');
        }
    }

    if (errors.length) {
        el.closest('.input').removeClass('input--ok').addClass('input--err');
        el.closest('form').find('button[type="submit"]').prop('disabled', 'disabled');
        return errors;
    } else {
        el.closest('form').find('button[type="submit"]').prop('disabled', '');

        if (!el.val()) {
            el.closest('.input').removeClass('input--err').removeClass('input--ok');
        } else {
            el.closest('.input').removeClass('input--err').addClass('input--ok');
            return true;
        }
    }
}