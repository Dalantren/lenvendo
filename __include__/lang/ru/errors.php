<?php
$MESS['ERROR_WRONG_PASSWORD'] = 'Не верный пароль';
$MESS['ERROR_WHILE_DELETE'] = 'Ошибка при удалении из БД';
$MESS['ERROR_NO_ELEMENT'] = 'Не найден элемент с таким id';

$MESS['ERROR_ALREADY_HAS_URL'] = 'Такой сайт уже есть в закладках';
$MESS['ERROR_ADD_BOOKMARK'] = 'Ошибка добавления закладки в БД';
$MESS['ERROR_CONNECTION_URL'] = 'Ошибка соединения с удаленным сервером';
$MESS['ERROR_BAD_HTTP_CODE'] = 'Неверный код ответа сервера';