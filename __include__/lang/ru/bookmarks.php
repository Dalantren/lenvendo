<?php
$MESS['DATE_ADD'] = 'Дата добавления';
$MESS['PAGE_TITLE'] = 'Заголовок страницы';
$MESS['PAGE_URL'] = 'Адрес страницы';
$MESS['FAVICON'] = 'Фавикон';
$MESS['META_DESCRIPTION'] = 'META description';
$MESS['META_KEYWORDS'] = 'META keywords';
$MESS['BACK_TO_LIST'] = 'Назад к списку';

$MESS['ADD_BOOKMARK'] = 'Добавить закладку';
$MESS['DELETE_BOOKMARK'] = 'Удалить закладку';
$MESS['BOOKMARKS_LIST_IS_EMPTY'] = 'Список закладок пуст';
$MESS['EXPORT_TO_EXCEL'] = 'Экспорт в Excel';

$MESS['ENTER_URL'] = 'Введите url';
$MESS['ENTER_PASSWORD'] = 'Введите пароль';
$MESS['ENTER_PASSWORD_AGAIN'] = 'Повторите пароль';
$MESS['ADD_BOOKMARK'] = 'Добавить закладку';

$MESS['POSITION_NUMBER'] = '№';