'use strict';

// init
var gulp        = require('gulp'),
    babel       = require('gulp-babel'),
    watch       = require('gulp-watch'),        // Наблюдение за изменениями файлов
    prefixer    = require('gulp-autoprefixer'), // Автоматически добавляет вендорные префиксы к CSS свойствам
    uglify      = require('gulp-uglify-es').default,      // Сжимать наш JS
    rigger      = require('gulp-rigger'),       // Позволяет импортировать один файл в другой простой конструкцией
    sass        = require('gulp-sass'),         // для компиляции нашего SCSS кода
    sourcemaps  = require('gulp-sourcemaps'),   // Для генерации css sourscemaps, помогает нам при отладке кода
    cssmin      = require('gulp-minify-css'),   // Сжатие CSS кода
    imagemin    = require('gulp-imagemin'),     // Сжатие картинок
    pngquant    = require('imagemin-pngquant'), // Сжатие картинок | работа с PNG
    plumber     = require('gulp-plumber'),      // Ловим ошибки, чтобы не прервался watch
    browsersync = require("browser-sync").create();


// write routs
var path = {
    build: {
        js:            'local/templates/lenvendo/js',
        styles:        'local/templates/lenvendo/css',
        images:        'local/templates/lenvendo/images/',
        fonts:         'local/templates/lenvendo/fonts/',
        fontBootstrap: 'local/templates/lenvendo/fonts/bootstrap/'
    },
    src: {
        js:                'src/js/*.*',
        styles:            'src/styles/*.*',
        componentStyles:   'local/components/**/*.scss',
        stylesPartials:    'src/styles/partials/',
        images:            'src/images/**/*.*',
        fonts:             'src/fonts/**/*.*',
        fontBootstrap:     'bower_components/bootstrap-sass/assets/fonts/bootstrap/*.*'
    },
    watch: {
        js:                 'src/js/**/*.js',
        componentStyles:    'local/components/**/*.scss',
        styles:             'src/styles/**/*.scss',
        images:             'src/images/**/*.*',
        fonts:              'src/fonts/**/*.*'
    }
};

// javascript
function jsbuild() {
    return gulp.src(path.src.js)
        //.pipe(babel({presets: ['@babel/preset-env']}))
        .pipe(plumber())
        .pipe(rigger())
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(plumber.stop())
        .pipe(gulp.dest(path.build.js))
        .pipe(browsersync.stream());
}
gulp.task('jsbuild', jsbuild);

// images
function imagebuild() {
    return gulp.src(path.src.images)
        .pipe(plumber())
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(plumber.stop())
        .pipe(gulp.dest(path.build.images))
}
gulp.task('imagebuild', imagebuild);

// move custom fonts to build
function fontsbuild() {
    return gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
}
gulp.task('fontsbuild', fontsbuild);

// styles
function stylebuild() {
    return gulp.src(path.src.styles)
        .pipe(plumber())
        .pipe(sass())
        .pipe(prefixer())
        .pipe(cssmin())
        .pipe(plumber.stop())
        .pipe(gulp.dest(path.build.styles))
        .pipe(browsersync.stream());
}

function componentStylesbuild() {
    return gulp.src(path.src.componentStyles)
        .pipe(plumber())
        .pipe(sass())
        .pipe(prefixer())
        .pipe(cssmin())
        .pipe(plumber.stop())
        .pipe(gulp.dest((file) => {
            return file.base;
        }))
        .pipe(browsersync.stream());
}

gulp.task('stylesbuild', gulp.series(stylebuild, componentStylesbuild));
//gulp.task('stylesbuild', stylebuild);

gulp.task('build', gulp.series(
    'jsbuild',
    //'imagebuild',
    'fontsbuild',
    'stylesbuild',
));

gulp.task('watch', function(){
    watch([path.watch.js], jsbuild);
    //watch([path.watch.images], imagebuild);
    watch([path.watch.fonts], fontsbuild);
    watch([path.watch.styles], stylebuild);
    watch([path.watch.componentStyles], componentStylesbuild)
});

gulp.task('default', gulp.series('build', 'watch'));