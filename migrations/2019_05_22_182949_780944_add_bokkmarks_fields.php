<?php

use Arrilot\BitrixMigrations\BaseMigrations\BitrixMigration;
use Arrilot\BitrixMigrations\Exceptions\MigrationException;
use Bitrix\Highloadblock as HL;

class AddBokkmarksFields20190522182949780944 extends BitrixMigration
{
    /**
     * Run the migration.
     *
     * @return mixed
     * @throws \Exception
     */
    public function up()
    {
        CModule::IncludeModule('highloadblock');
        $hiblock = HL\HighloadBlockTable::resolveHighloadblock('bookmarks');
        /**
         * Добавление пользовательского свойства
         */
        $oUserTypeEntity  = new CUserTypeEntity();
        
        $aUserFields  = [
            [
                'ENTITY_ID'         => 'HLBLOCK_' . $hiblock['ID'],
                'FIELD_NAME'        => 'UF_BOOKMARK_DATE_ADD',
                'USER_TYPE_ID'      => 'datetime',
                'SORT'              => 500,
                'MULTIPLE'          => 'N',
                'MANDATORY'         => 'Y',
                'SHOW_FILTER'       => 'N',
                'SHOW_IN_LIST'      => 'Y',
                'EDIT_IN_LIST'      => 'N',
                'IS_SEARCHABLE'     => 'N',
                'SETTINGS'          => [
                    'DEFAULT_VALUE' => [
                        'TYPE'  => 'NOW',
                        'VALUE' => ''
                    ]
                ],
                'EDIT_FORM_LABEL'   => [
                    'ru'    => 'Дата добавления закладки',
                    'en'    => 'Bookmarke date add',
                ],
                'LIST_COLUMN_LABEL' => [
                    'ru'    => 'Дата добавления закладки',
                    'en'    => 'Bookmarke date add',
                ],
            ],
            [
                'ENTITY_ID'         => 'HLBLOCK_' . $hiblock['ID'],
                'FIELD_NAME'        => 'UF_BOOKMARK_URL',
                'USER_TYPE_ID'      => 'string',
                'SORT'              => 500,
                'MULTIPLE'          => 'N',
                'MANDATORY'         => 'Y',
                'SHOW_FILTER'       => 'N',
                'SHOW_IN_LIST'      => 'Y',
                'EDIT_IN_LIST'      => 'Y',
                'IS_SEARCHABLE'     => 'Y',
                'SETTINGS'          => [
                    'SIZE'          => '20',
                    'ROWS'          => '1',
                ],
                'EDIT_FORM_LABEL'   => [
                    'ru'    => 'URL закладки',
                    'en'    => 'Bookmark url',
                ],
                'LIST_COLUMN_LABEL' => [
                    'ru'    => 'URL закладки',
                    'en'    => 'Bookmark url',
                ],
            ],
            [
                'ENTITY_ID'         => 'HLBLOCK_' . $hiblock['ID'],
                'FIELD_NAME'        => 'UF_BOOKMARK_FAVICON',
                'USER_TYPE_ID'      => 'file',
                'SORT'              => 500,
                'MULTIPLE'          => 'N',
                'MANDATORY'         => 'N',
                'SHOW_FILTER'       => 'N',
                'SHOW_IN_LIST'      => 'Y',
                'EDIT_IN_LIST'      => 'N',
                'IS_SEARCHABLE'     => 'N',
                'SETTINGS'          => [],
                'EDIT_FORM_LABEL'   => [
                    'ru'    => 'Favicon закладки',
                    'en'    => 'Bookmark favicon',
                ],
                'LIST_COLUMN_LABEL' => [
                    'ru'    => 'Favicon закладки',
                    'en'    => 'Bookmark favicon',
                ],
            ],
            [
                'ENTITY_ID'         => 'HLBLOCK_' . $hiblock['ID'],
                'FIELD_NAME'        => 'UF_BOOKMARK_DESCR',
                'USER_TYPE_ID'      => 'string',
                'SORT'              => 500,
                'MULTIPLE'          => 'N',
                'MANDATORY'         => 'N',
                'SHOW_FILTER'       => 'S',
                'SHOW_IN_LIST'      => 'Y',
                'EDIT_IN_LIST'      => 'Y',
                'IS_SEARCHABLE'     => 'Y',
                'SETTINGS'          => [
                    'DEFAULT_VALUE' => '',
                    'SIZE'          => '20',
                    'ROWS'          => '3',
                    'MIN_LENGTH'    => '0',
                    'MAX_LENGTH'    => '0',
                    'REGEXP'        => '',
                ],
                'EDIT_FORM_LABEL'   => [
                    'ru'    => 'META Description закладки',
                    'en'    => 'Bookmark description',
                ],
                'LIST_FILTER_LABEL' => [
                    'ru'    => 'META Description закладки',
                    'en'    => 'Bookmark description',
                ],
                'LIST_COLUMN_LABEL' => [
                    'ru'    => 'META Description закладки',
                    'en'    => 'Bookmark description',
                ],
            ],
            [
                'ENTITY_ID'         => 'HLBLOCK_' . $hiblock['ID'],
                'FIELD_NAME'        => 'UF_BOOKMARK_KEYWORDS',
                'USER_TYPE_ID'      => 'string',
                'SORT'              => 500,
                'MULTIPLE'          => 'N',
                'MANDATORY'         => 'N',
                'SHOW_FILTER'       => 'N',
                'SHOW_IN_LIST'      => 'Y',
                'EDIT_IN_LIST'      => 'Y',
                'IS_SEARCHABLE'     => 'Y',
                'SETTINGS'          => [
                    'DEFAULT_VALUE' => '',
                    'SIZE'          => '20',
                    'ROWS'          => '3',
                    'MIN_LENGTH'    => '0',
                    'MAX_LENGTH'    => '0',
                    'REGEXP'        => '',
                ],
                'EDIT_FORM_LABEL'   => [
                    'ru'    => 'META Keywords закладки',
                    'en'    => 'Bookmark keywords',
                ],
                'LIST_FILTER_LABEL' => [
                    'ru'    => 'META Keywords закладки',
                    'en'    => 'Bookmark keywords',
                ],
                'LIST_COLUMN_LABEL' => [
                    'ru'    => 'META Keywords закладки',
                    'en'    => 'Bookmark keywords',
                ],
            ],
            [
                'ENTITY_ID'         => 'HLBLOCK_' . $hiblock['ID'],
                'FIELD_NAME'        => 'UF_BOOKMARK_TITLE',
                'USER_TYPE_ID'      => 'string',
                'SORT'              => 500,
                'MULTIPLE'          => 'N',
                'MANDATORY'         => 'N',
                'SHOW_FILTER'       => 'N',
                'SHOW_IN_LIST'      => 'Y',
                'EDIT_IN_LIST'      => 'Y',
                'IS_SEARCHABLE'     => 'Y',
                'SETTINGS'          => [
                    'DEFAULT_VALUE' => '',
                    'SIZE'          => '20',
                    'ROWS'          => '1',
                    'MIN_LENGTH'    => '0',
                    'MAX_LENGTH'    => '0',
                    'REGEXP'        => '',
                ],
                'EDIT_FORM_LABEL'   => [
                    'ru'    => 'META Title закладки',
                    'en'    => 'Bookmark title',
                ],
                'LIST_FILTER_LABEL' => [
                    'ru'    => 'META Keywords закладки',
                    'en'    => 'Bookmark keywords',
                ],
                'LIST_COLUMN_LABEL' => [
                    'ru'    => 'META Title закладки',
                    'en'    => 'Bookmark title',
                ],
            ],
        ];
        
        foreach ($aUserFields as $field) {
            if (!$oUserTypeEntity->Add( $field )) {
                $this->down();
                throw new MigrationException('Ошибка при добавлении ' . $field['FIELD_NAME']);
            }
        }
    }

    /**
     * Reverse the migration.
     *
     * @return mixed
     * @throws \Exception
     */
    public function down()
    {
        $oUserTypeEntity  = new CUserTypeEntity();

        $fieldsToDelete = [
            'UF_BOOKMARK_DATE_ADD',
            'UF_BOOKMARK_URL',
            'UF_BOOKMARK_FAVICON',
            'UF_BOOKMARK_TITLE',
            'UF_BOOKMARK_DESCR',
            'UF_BOOKMARK_KEYWORDS'
        ];

        foreach ($fieldsToDelete as $fieldCode) {
            if ($field = CUserTypeEntity::GetList([], ['FIELD_NAME' => $fieldCode])->Fetch()) {
                $oUserTypeEntity->delete($field['ID']);
            }            
        }        
    }
}
