<?php

use Arrilot\BitrixMigrations\BaseMigrations\BitrixMigration;
use Arrilot\BitrixMigrations\Exceptions\MigrationException;
use Bitrix\Highloadblock as HL;

class AddBookmarskHiblock20190522175047211596 extends BitrixMigration
{
    /**
     * Run the migration.
     *
     * @return mixed
     * @throws \Exception
     */
    public function up()
    {
        CModule::IncludeModule('highloadblock');

        $res = HL\HighloadBlockTable::add([
            'NAME'          => 'Bookmarks',
            'TABLE_NAME'    => 'bookmarks'
        ]);

        if (!$res->isSuccess()) {
            throw new MigrationException($res->getErrorMessages());
        }
    }

    /**
     * Reverse the migration.
     *
     * @return mixed
     * @throws \Exception
     */
    public function down()
    {
        CModule::IncludeModule('highloadblock');

        $hiblock = HL\HighloadBlockTable::resolveHighloadblock('bookmarks');
        //throw new MigrationException($hiblock['ID'] . ' === ' . $hiblock['TABLE_NAME']);
        $res = HL\HighloadBlockTable::delete($hiblock['ID']);
    }
}
