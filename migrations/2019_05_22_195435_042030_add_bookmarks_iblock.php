<?php

use Arrilot\BitrixMigrations\BaseMigrations\BitrixMigration;
use Arrilot\BitrixMigrations\Exceptions\MigrationException;

class AddBookmarksIblock20190522195435042030 extends BitrixMigration
{
    /**
     * Run the migration.
     *
     * @return mixed
     * @throws MigrationException
     */
    public function up()
    {
        $ib = new CIBlock;

        $iblockId = $ib->add([
            'NAME'              => 'Закладки',
            'CODE'              => 'bookmarks',
            'SITE_ID'           => 's1',
            'IBLOCK_TYPE_ID'    => 'content',
            'VERSION'           => 2,
            'GROUP_ID'          => ['2' =>'R'],
            'LIST_PAGE_URL'     => '#SITE_DIR#/#IBLOCK_CODE#/',
            'DETAIL_PAGE_URL'   => '#SITE_DIR#/#IBLOCK_CODE#/#ELEMENT_ID#',
            'ELEMENT_NAME'      => 'Закладка',
            'ELEMENTS_NAME'     => 'Закладки',
            'ELEMENT_ADD'       => 'Добавить закладку',
            'ELEMENT_EDIT'      => 'Изменить закладку',
            'ELEMENT_DELETE'    => 'Удалить закладку'
        ]);

        if (!$iblockId) {
            throw new MigrationException('Ошибка при добавлении инфоблока '.$ib->LAST_ERROR);
        }

        // свойства
        $propId = $this->addIblockElementProperty([
            'NAME' => 'URL Закладки',
            'SORT' => 500,
            'CODE' => 'URL',
            'PROPERTY_TYPE' => 'S',
            'MULTIPLE'  => 'N',
            'IS_REQUIRED' => 'Y',
            'IBLOCK_ID' => $iblockId
        ]);

        $propId = $this->addIblockElementProperty([
            'NAME' => 'Favicon закладки',
            'SORT' => 500,
            'CODE' => 'FAVICON',
            'PROPERTY_TYPE' => 'F',
            'MULTIPLE'  => 'N',
            'IS_REQUIRED' => 'N',
            'IBLOCK_ID' => $iblockId
        ]);

        $propId = $this->addIblockElementProperty([
            'NAME' => 'Title Закладки',
            'SORT' => 500,
            'CODE' => 'TITLE',
            'PROPERTY_TYPE' => 'S',
            'MULTIPLE'  => 'N',
            'IS_REQUIRED' => 'N',
            'IBLOCK_ID' => $iblockId
        ]);

        $propId = $this->addIblockElementProperty([
            'NAME' => 'META Keywords Закладки',
            'SORT' => 500,
            'CODE' => 'KEYWORDS',
            'PROPERTY_TYPE' => 'S',
            'MULTIPLE'  => 'N',
            'IS_REQUIRED' => 'N',
            'IBLOCK_ID' => $iblockId
        ]);

        $propId = $this->addIblockElementProperty([
            'NAME' => 'META Description Закладки',
            'SORT' => 500,
            'CODE' => 'DESCRIPTION',
            'PROPERTY_TYPE' => 'S',
            'MULTIPLE'  => 'N',
            'IS_REQUIRED' => 'N',
            'IBLOCK_ID' => $iblockId
        ]);
    }

    /**
     * Reverse the migration.
     *
     * @return mixed
     */
    public function down()
    {
        $this->deleteIblockByCode('bookmarks');
    }
}
