<?php

use Arrilot\BitrixMigrations\BaseMigrations\BitrixMigration;
use Arrilot\BitrixMigrations\Exceptions\MigrationException;

class AddContentIblockType20190522195236888122 extends BitrixMigration
{
    /**
     * Run the migration.
     *
     * @return mixed
     */
    public function up()
    {
        CModule::IncludeModule('iblock');
        $cbt = new CIBlockType;
        $cbtRes = $cbt->Add([
            'ID'        => 'content',
            'SECTIONS'  => 'Y',
            'IN_RSS'    => 'N',
            'SORT'      => 100,
            'LANG'      => [
                'ru'    => [
                            'NAME' => 'Контент',
                        ]
            ]
        ]);

        if (!$cbtRes) {
            throw new MigrationException('Ошибка при добавлении типа инфоблока '.$cbt->LAST_ERROR);
        }
    }

    /**
     * Reverse the migration.
     *
     * @return mixed
     */
    public function down()
    {
        CModule::IncludeModule('iblock');

        global $DB;
        $DB->StartTransaction();

        if (!CIBlockType::Delete('content')) {
            $DB->Rollback();
            throw new MigrationException('Ошибка при удалении типа инфоблока Контент');
        }

        $DB->Commit();
    }
}
