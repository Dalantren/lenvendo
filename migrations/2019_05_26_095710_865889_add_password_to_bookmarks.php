<?php

use Arrilot\BitrixMigrations\BaseMigrations\BitrixMigration;
use Arrilot\BitrixMigrations\Exceptions\MigrationException;

class AddPasswordToBookmarks20190526095710865889 extends BitrixMigration
{
    /**
     * Run the migration.
     *
     * @return mixed
     * @throws \Exception
     */
    public function up()
    {
        $bookmarksIblockId = $this->getIblockIdByCode('bookmarks');
    
        $propId = $this->addIblockElementProperty([
            'NAME' => 'Пароль для удаления',
            'SORT' => 500,
            'CODE' => 'PASSWORD',
            'PROPERTY_TYPE' => 'S',
            'MULTIPLE'  => 'N',
            'IS_REQUIRED' => 'N',
            'IBLOCK_ID' => $bookmarksIblockId
        ]);
    }

    /**
     * Reverse the migration.
     *
     * @return mixed
     * @throws \Exception
     */
    public function down()
    {
        $bookmarksIblockId = $this->getIblockIdByCode('bookmarks');
        
        $this->deleteIblockElementPropertyByCode($bookmarksIblockId, 'PASSWORD');
    }
}
