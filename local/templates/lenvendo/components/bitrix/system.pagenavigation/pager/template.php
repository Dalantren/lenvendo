<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}
global $pager_ajax_component_id;

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "compid=press&amp;");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>

<div class="pager" data-pager="<?=$arResult['NavPageNomer']?>">
    <a
        <?php if ($arResult['NavPageNomer'] > 1) { ?>
            data-ajax-params="<?=$strNavQueryString?>&page=<?=($arResult["NavPageNomer"]-1)?>"
        <?php } ?>
        class="pager__page pager__page--prev <?=$arResult["NavPageNomer"] > 1 ? ' pager__page--prev-active' : ''?>"
    ><svg width="6" height="10" viewBox="0 0 6 10" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M4.60564 0.230794C4.92502 -0.076931 5.44149 -0.076931 5.76087 0.230794C6.07971 0.538519 6.07971 1.03719 5.76087 1.34491L1.97143 4.99974L5.76087 8.65509C6.07971 8.96281 6.07971 9.46148 5.76087 9.76921C5.44149 10.0769 4.92502 10.0769 4.60564 9.76921L0.239127 5.5568C-0.0797086 5.24907 -0.0797086 4.7504 0.239127 4.44268L4.60564 0.230794Z" fill="#0D4A75"/>
        </svg>
    </a>
    
    
    <a
        data-ajax-params="<?=$strNavQueryString?>&page=1"
        class="pager__page <?=$arResult["NavPageNomer"] == 1 ? ' pager__page--active' : ''?>"
    >1</a>
    
    
    
    <?php
    $relative_pages = 2;
    for ($page = $arResult['NavPageNomer'] - $relative_pages; $page <= $arResult['NavPageNomer'] + $relative_pages; $page ++) {
        if ($page > 1 && $page < $arResult['nEndPage']) { ?>
            <a
                data-ajax-params="<?=$strNavQueryString?>&page=<?=$page?>"
                class="pager__page <?=$page == $arResult['NavPageNomer'] ? 'pager__page--active' : ''?>"
            ><?=$page?></a>
        <?php }
    } ?>
    
    <a
        data-ajax-params="<?=$strNavQueryString?>&page=<?=$arResult["NavPageCount"]?>"
        class="pager__page <?=$arResult["NavPageNomer"] == $arResult["NavPageCount"] ? ' pager__page--active' : ''?>"
    ><?=$arResult["NavPageCount"]?></a>
    
    <a
        <?php if ($arResult['NavPageNomer'] != $arResult['nEndPage']) { ?>
            data-ajax-params="<?=$strNavQueryString?>&page=<?=($arResult["NavPageNomer"]+1)?>"
        <?php } ?>
        class="pager__page pager__page--next <?=$arResult['NavPageNomer'] != $arResult['nEndPage'] ? ' pager__page--next-active' : ''?>"
    ><svg width="6" height="10" viewBox="0 0 6 10" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M1.39436 9.76921C1.07498 10.0769 0.558508 10.0769 0.239126 9.76921C-0.0797088 9.46148 -0.0797088 8.96281 0.239126 8.65509L4.02857 5.00026L0.239126 1.34491C-0.0797088 1.03719 -0.0797088 0.538518 0.239126 0.230794C0.558508 -0.0769312 1.07498 -0.0769312 1.39436 0.230794L5.76087 4.4432C6.07971 4.75093 6.07971 5.2496 5.76087 5.55732L1.39436 9.76921Z" fill="#0D4A75"/>
        </svg>
    </a>
</div>


