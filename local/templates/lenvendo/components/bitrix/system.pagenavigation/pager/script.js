$(document).ready(function() {

    //Ajax переключение страниц пагинации
    $('body').on('click', '.pager__page', function() {
        let ajaxBlock = $(this).closest('.js-ajax-block');

        if ($(this).hasClass('pager__page--active')) {
            return false;
        }

        $.ajax({
            data: $(this).data('ajaxParams') + '&' + ajaxBlock.data('ajax'),
            success: function (resp) {
                ajaxBlock.replaceWith(resp);
            }
        });
    });

});

