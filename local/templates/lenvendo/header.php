<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

define("ROOT", $_SERVER['DOCUMENT_ROOT']);
use Bitrix\Main\Page\Asset;
$asset = Asset::getInstance();
?>
<!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>">
<head>
    <title><?php $APPLICATION->ShowTitle() ?></title>
    <meta charset="UTF-8">
    <meta author="dalantren">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
    <?php $asset->addCss(SITE_TEMPLATE_PATH . '/css/main.css');?>
    <?php
    $APPLICATION->ShowHead();    
    $APPLICATION->ShowPanel();
    ?>
</head>
<body class="page page_<?=LANGUAGE_ID?> ">
<header class="page__top header">
    <div class="header__inner wrap">
        <h1 class="header__title title title--center">
        <?php $APPLICATION->IncludeComponent(
            "bitrix:main.include","",
            ["AREA_FILE_SHOW" => "file", "PATH" => "/__include__/header/title.php"]
        );?>
        </h1>
    </div>
</header>
<main class="page__wrapper">
    <div class="page__middle">
