<?php

namespace Local\Dalantren;

trait BaseFunctions
{
    use \Bex\Bbc\Traits\Elements;
    
    /**
     * Set number of elements from ELEMENTS_COUNT parameter
     */
    protected function setParamsNav()
    {
        if ($this->arParams['ELEMENTS_COUNT']) {
            $this->navStartParams['nPageSize'] = (int)$this->arParams['ELEMENTS_COUNT'];
        }
    }
    
    /**
     * Set Filter parameters from component parameters
     */
    protected function setParamsFilters()
    {
        if ($this->arParams['IBLOCK_TYPE']) {
            $this->filterParams['IBLOCK_TYPE'] = $this->arParams['IBLOCK_TYPE'];
        }
        
        if ($this->arParams['IBLOCK_ID']) {
            $this->filterParams['IBLOCK_ID'] = $this->arParams['IBLOCK_ID'];
        } elseif ($this->arParams['IBLOCK_CODE']) {
            $this->filterParams['IBLOCK_CODE'] = $this->arParams['IBLOCK_CODE'];
        }
        
        if ($this->arParams['SECTION_CODE']) {
            $this->filterParams['SECTION_CODE'] = $this->arParams['SECTION_CODE'];
        } elseif ($this->arParams['SECTION_ID']) {
            $this->filterParams['SECTION_ID'] = $this->arParams['SECTION_ID'];
        }
        
        if ($this->arParams['INCLUDE_SUBSECTIONS'] === 'Y') {
            $this->filterParams['INCLUDE_SUBSECTIONS'] = 'Y';
        }
        
        if ($this->arParams['ELEMENT_CODE']) {
            $this->filterParams['CODE'] = $this->arParams['ELEMENT_CODE'];
        } elseif ($this->arParams['ELEMENT_ID']) {
            $this->filterParams['ID'] = $this->arParams['ELEMENT_ID'];
        }
        
        if ($this->arParams['CHECK_PERMISSIONS']) {
            $this->filterParams['CHECK_PERMISSIONS'] = $this->arParams['CHECK_PERMISSIONS'];
        }
        
        if (!isset($this->filterParams['ACTIVE'])) {
            $this->filterParams['ACTIVE'] = 'Y';
        }
        
        if (strlen($this->arParams['EX_FILTER_NAME']) > 0
            && preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $this->arParams['EX_FILTER_NAME'])
            && is_array($GLOBALS[$this->arParams['EX_FILTER_NAME']])
        ) {
            $this->filterParams = array_merge_recursive($this->filterParams, $GLOBALS[$this->arParams['EX_FILTER_NAME']]);
            
            $this->addCacheAdditionalId($GLOBALS[$this->arParams['EX_FILTER_NAME']]);
        }
    }
    
    /**
     * Returns prepare parameters of sort of the component
     *
     * @param array $additionalFields Additional fields for sorting
     * @return array
     */
    public function getParamsSort($additionalFields = [])
    {
        $this->arParams['SORT_BY_1'] = trim($this->arParams['SORT_BY_1']);
        
        if (strlen($this->arParams['SORT_BY_1']) <= 0)
        {
            $this->arParams['SORT_BY_1'] = 'ACTIVE_FROM';
        }
        
        if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $this->arParams['SORT_ORDER_1']))
        {
            $this->arParams['SORT_ORDER_1'] = 'DESC';
        }
        
        if (strlen($this->arParams['SORT_BY_2']) <= 0)
        {
            $this->arParams['SORT_BY_2'] = 'SORT';
        }
        
        if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $this->arParams['SORT_ORDER_2']))
        {
            $this->arParams['SORT_ORDER_2'] = 'ASC';
        }
        
        $fields = [
            $this->arParams['SORT_BY_1'] => $this->arParams['SORT_ORDER_1'],
            $this->arParams['SORT_BY_2'] => $this->arParams['SORT_ORDER_2']
        ];
        
        if (is_array($additionalFields) && !empty($additionalFields))
        {
            $fields = array_merge($additionalFields, $fields);
        }
        return $fields;
    }
    
    /**
     * Returns prepare file URL and set dimensions if $width and $height exists
     *
     * @param integer $fileId ID of file in Bitrix environment
     * @param integer $width width of picture for resize
     * @param integer $height height of picture for resize
     * @return string
     */
    protected function getFileUrl ($fileId, $width = 0, $height = 0) {
        
        if ($width > 0 && $height > 0) {
            return \CFile::ResizeImageGet(
                \CFile::GetFileArray($fileId),
                ['width' => $width, 'height' => $height],
                BX_RESIZE_IMAGE_EXACT,
                false,
                false,
                false,
                100
            );
        } else {
            return \CFile::GetPath($fileId);
        }
    }
    
    /**
     * Returns prepare files URL for array of files
     *
     * @param array $docs IDs array of files in Bitrix environment
     * @return array
     */
    protected function getDocumentsArr($docs)
    {
        $result = [];
        
        if(!is_array($docs)) {
            $docs = explode(',', $docs);
        }
        
        foreach ($docs as $doc_id) {
            $ob_file = \CFile::GetById($doc_id);
            $file = $ob_file->GetNext();
            $result[$doc_id] = $file;
            //Расширение файла
            $result[$doc_id]['EXTENSION'] = pathinfo($file['FILE_NAME'], PATHINFO_EXTENSION);
            
            //Папка с картинками расширений
            $extensionsImgDir = SITE_TEMPLATE_PATH . '/img/svg/extensions/';
            //Пытаемся получить файлик с картинкой для известного расширения. Если нет - подключаем unknown
            $extensionImgSrc  = $extensionsImgDir . strtolower($result[$doc_id]['EXTENSION']) . '.svg';
            if (file_exists( $_SERVER['DOCUMENT_ROOT'] . $extensionImgSrc)) {
                $result[$doc_id]['EXTENSION_IMG_SRC'] = $extensionImgSrc;
            } else {
                $result[$doc_id]['EXTENSION_IMG_SRC'] = $extensionsImgDir . 'unknown.svg';
            }
            $result[$doc_id]['FILE_NAME_NO_EXT'] =
                $this->mb_ucfirst(strtolower(pathinfo($file['ORIGINAL_NAME'],  PATHINFO_FILENAME)), 'utf-8');
            $result[$doc_id]['SRC'] = \CFile::GetPath($doc_id);
        }
        
        return $result;
    }
    
    /**
     * Returns a string's first character uppercase in multibyte encoding
     *
     * @param string $string string to explode
     * @param string $encoding type of encoding result
     * @return array
     */
    protected function mb_ucfirst($string, $encoding)
    {
        $strlen = mb_strlen($string, $encoding);
        $firstChar = mb_substr($string, 0, 1, $encoding);
        $then = mb_substr($string, 1, $strlen - 1, $encoding);
        return mb_strtoupper($firstChar, $encoding) . $then;
    }
    
    /**
     * Returns an array of link buttons in Bitrix environment
     *
     * @param integer $iblock_id Infoblock ID
     * @param integer $elId Element ID
     * @return array
     */
    protected function getButtons($iblockId, $elId) {
        
        $ar_buttons = \CIBlock::GetPanelButtons($iblockId, $elId);

        $block['ADD_LINK'] = $ar_buttons['edit']['add_element']['ACTION_URL'];
        $block['EDIT_LINK'] = $ar_buttons['edit']['edit_element']['ACTION_URL'];
        $block['DELETE_LINK'] = $ar_buttons['edit']['delete_element']['ACTION_URL'];
        $block['ADD_LINK_TEXT'] = $ar_buttons['edit']['add_element']['TEXT'];
        $block['EDIT_LINK_TEXT'] = $ar_buttons['edit']['edit_element']['TEXT'];
        $block['DELETE_LINK_TEXT'] = $ar_buttons['edit']['delete_element']['TEXT'];
        
        return $block;
    }
    
    /**
     * Returns a prepared for using element
     *
     * @param CIBlockResult $element Bitrix infoblock element
     * @return CIBlockResult
     */
    protected function prepareElement($element)
    {
        //Получаем url детальной картинки
        if (!empty($element['DETAIL_PICTURE'])) {
            $element['DETAIL_IMG_SRC'] = $this->getFileUrl($element['DETAIL_PICTURE']);
        }
    
        //Получаем url картинки анонса
        if (!empty($element['PREVIEW_PICTURE'])) {
            $element['PREVIEW_IMG_SRC'] = $this->getFileUrl($element['PREVIEW_PICTURE']);
        }
    
        //Обрабатываем свойства элемента
        foreach ($element['PROPS'] as &$prop) {
    
            //Для типа файл подготавливаем url файлов и прочее
            if ($prop['PROPERTY_TYPE'] == 'F') {
                if ($prop['MULTIPLE'] == 'N') {
                    $prop['SRC'] = $this->getFileUrl($prop['VALUE']);
                } else {
                    $prop['VALUES_EXT'] = $this->getDocumentsArr($prop['VALUE']);
                }
            }
            
            //Для типа свойства "Привязка к элементам ИБ" получаем эти элементы
            if ($prop['PROPERTY_TYPE'] == 'E') {
                $elementsRes = \CIBlockElement::GetList(
                    ['SORT' => 'ASC'],
                    [
                        'IBLOCK_ID' => $prop['LINK_IBLOCK_ID'],
                        'ID' => $prop['VALUE']
                    ],
                    false, false, []);

                while($objRes = $elementsRes->GetNextElement()) {
                    if ($arElement = $this->processingElementsResult($objRes)) {
                        $prop['EXT'][] = $arElement;
                    }
                }
            }
        }
        
        return $element;
    }
    
    /**
     * Trying to delete a bookmark.
     * Return ['status' => true] in case of success or
     * ['status' => false, 'msg' => message] in case of error
     *
     * @param integer $id Bitrix infoblock element ID
     * @param string $pass Password to delete element
     * @return array
     */
    public function deleteBookmark($id, $pass = '')
    {
        $id = (int)$id;
        $el = \CIBlockElement::GetList([],
            [
                'ID' => $id,
                'IBLOCK_CODE' => 'bookmarks'
            ], false, false,[]
        )->GetNextElement(false, false);
        
        if (!$el) {
            return ['status' => false, 'msg' => \Bitrix\Main\Localization\Loc::getMessage('ERROR_NO_ELEMENT')];
        }
        
        $bookmark = $this->processingElementsResult($el);
        
        if ($bookmark['PROPS']['PASSWORD']['VALUE'] && !password_verify($pass, $bookmark['PROPS']['PASSWORD']['VALUE'])) {
            return ['status' => false, 'msg' => \Bitrix\Main\Localization\Loc::getMessage('ERROR_WRONG_PASSWORD')];
        }
        
        if(\CIBlockElement::delete($id)) {
            return ['status' => true];
        } else {
            return ['status' => false, 'msg' => \Bitrix\Main\Localization\Loc::getMessage('ERROR_WHILE_DELETE')];
        }
    }
    
    /**
     * Redirect on 404 page
     */
    public function return404()
    {
        LocalRedirect ("/404.php");
    }
    
    public function fileGetContentsCurl($url)
    {
        $ch = curl_init();
    
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    
        $data = curl_exec($ch);
        curl_close($ch);
    
        return $data;
    }
    
    public function getSiteUrl()
    {
        return $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'];
    }
}