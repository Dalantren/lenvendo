<?php

AddEventHandler(
    '',
    'onAfterTwigTemplateEngineInited',
    function (\Twig_Environment $engine) {
        
        $engine->addExtension(new Kint\Twig\TwigExtension());
        
        // Добавляем кастомные фильтры для twig
        
        //Русские месяцы
        $filter = new \Twig_SimpleFilter('rusDate', function ($date) {
            $months = [
                '01' => 'января',
                '02' => 'февраля',
                '03' => 'марта',
                '04' => 'апреля',
                '05' => 'мая',
                '06' => 'июня',
                '07' => 'июля',
                '08' => 'августа',
                '09' => 'сентября',
                '10' => 'октября',
                '11' => 'ноября',
                '12' => 'декабря'
            ];
            $timestamp = strtotime($date);
            return implode(
                ' ', [
                        date('d', $timestamp),
                        $months[date('m', $timestamp)],
                        date('Y', $timestamp)
                ]);
        });
        $engine->addFilter($filter);
    
        // Обрезка мультибайтовой строки
        $filter = new \Twig_SimpleFilter('sslice', function ($str, $from, $num = NULL) {
            return mb_substr($str, $from, $num);
        });
        $engine->addFilter($filter);
    
        //Плюрализация рус одного слова
        /* варианты написания для количества 1, 2 и 5 */
        $filter = new \Twig_SimpleFilter('plural', function ($number, $word_1, $word_2, $word_5) {
            return $number . ' ' . [$word_1, $word_2, $word_5][$number % 10 == 1 && $number % 100 != 11? 0 : ($number % 10 >= 2 && $number % 10 <= 4 && $number % 100 < 10 || $number % 100 >= 20 ? 1 : 2)];
        });
        $engine->addFilter($filter);
    
        //Плюрализация рус слова до и после
        //$before, $after - массивы с тремя значениями
        $filter = new \Twig_SimpleFilter('plural_d', function ($number, $before, $after) {
            $cases = [2, 0, 1, 1, 1, 2];
            return
                $before[($number % 100 > 4 && $number % 100 < 20)? 2 : $cases[min($number % 10, 5)]]
                .' '.$number.' '.
                $after[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
        });
        $engine->addFilter($filter);
    
    
        //implode - чутка улучшенный join от twig
        $filter = new \Twig_SimpleFilter('implode', function ($arr, $separator) {
            return trim(trim(implode($separator, $arr)), $separator);
        });
        $engine->addFilter($filter);
        
        return new \Bitrix\Main\EventResult(\Bitrix\Main\EventResult::SUCCESS, array($engine));
    }
);
