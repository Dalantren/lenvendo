<?php

if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

use Bex\Bbc\Basis;
use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (!\Bitrix\Main\Loader::includeModule('bex.bbc')) return false;


class BookmarksDetail extends Basis
{
    // Типаж для работы с элементами инфоблоков и различные соцфичи
    use \Local\Dalantren\BaseFunctions;
    
    
    // Модули, которые будут подключены
    protected $needModules = ['iblock'];
    
    protected $checkParams = [
        'IBLOCK_CODE'   => ['type' => 'string'],
    ];
    
    protected $cacheTemplate = false;
    
    protected function executeMain()
    {
        //Устанавливаем параметры фильтра из настроек компонента
        $this->setParamsFilters();
    
        $request = Application::getInstance()->getContext()->getRequest();
        
        if ($this->isAjax()) {
            //Если пришел ajax запрос на удаление - пытаемся удалить и возвращаем json ответ
            if ($request->get('action') == 'delete' && $request->get('id')) {
                $res = $this->deleteBookmark($request->get('id'), $request->get('pass'));
                echo json_encode($res);
                exit;
            }
        }

        if(!$request->get('EL_ID')) {
            throw new Bitrix\Main\SystemException(
                \Bitrix\Main\Localization\Loc::getMessage('ERROR_EMPTY_BOOKMARK_ID')
            );
        }

        $this->arResult['EL'] = [];
        $rsElements = \CIblockElement::GetByID((int)$request->get('EL_ID'));

        $processingMethod = $this->getProcessingMethod();
        while ($element = $rsElements->$processingMethod(false, false)) {
            if ($arElement = $this->processingElementsResult($element)) {
                $this->arResult['EL'] = $arElement;
            }
        }
    }
    
    public function prepareElementsResult($el)
    {
        $el = $this->prepareElement($el);
        $el['BUTTONS'] = $this->getButtons($el['IBLOCK_ID'], $el['ID']);
    
        return $el;
    }
}


