$(document).ready(function(){

    let $body = $('body');

    //Открытие формы для добавления закладки
    $body.on('click', '.js-show-add', () => {

        $.ajax({
            data: $('#bookmarks').data('ajax') + '&tpl=add_form',
            success: (resp) => {
                $body.find('#bookmarks').replaceWith(resp);
            }
        })
    })

    $body.on('click', '.js-export-excel', () => {

        $.ajax({
            data: $('#bookmarks').data('ajax') + '&action=exportExcel',
            success: (resp) => {
                let response = JSON.parse(resp);
                if (response.status && response.filePath) {
                    window.open(response.filePath);
                }
            }
        })

    });

    //Установка сортировки в списке закладок
    $body.on('click', '.bookmarks-list__row--head .bookmark-preview__field', function() {
        if (!$(this).data('sort') || !$(this).data('order')) {
            return;
        }
        let sort = $(this).data('sort');
        let order = $(this).data('order') === 'asc' ? 'desc' : 'asc';

        $.ajax({
            data: $('#bookmarks').data('ajax') + '&sort=' + $(this).data('sort') + `&order=${order}` + `&page=` + $('.pager').data('pager'),
            success: (resp) => {
                $body.find('#bookmarks').replaceWith(resp);
                let $this = $body.find(`.bookmarks-list__row--head .bookmark-preview__field[data-sort=${sort}]`);

                $this.data('order', order);
                if (order === 'asc') {
                    $this.removeClass('bookmark-preview__field--desc').addClass('bookmark-preview__field--asc');
                } else {
                    $this.removeClass('bookmark-preview__field--asc').addClass('bookmark-preview__field--desc');
                }
            }
        })
    })

    //Запрос на добавление закладки
    $body.on('submit', '#add-bookmark', function(e) {

        e.preventDefault();

        let $input = $(this).find('input[type="url"]').closest('.input'),
            $errorText = $(this).find('.add-form__error-text');

        //Предварительно очищаем поле для вывода ошибок
        $errorText.text('');

        $.ajax({
            data: $(this).data('ajax') + '&' + $(this).serialize(),
            dataType: 'json',
            success: (resp) => {
                if (!resp.status) {
                    //Выводим сообщение об ошибке если не удалось добавить
                    $input.removeClass('input--ok').addClass('input--err');
                    $errorText.text(resp.msg);
                } else {
                    //Редирект на детальную страницу закладки
                    window.location.href = `/bookmarks/${resp.newId}`;
                }
            }
        })
    });
});