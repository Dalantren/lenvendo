<?php

if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

use Bex\Bbc\Basis;
use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use TrueBV\Punycode;

Loc::loadMessages(__FILE__);

if (!\Bitrix\Main\Loader::includeModule('bex.bbc')) return false;


class BookmarksList extends Basis
{
    use \Local\Dalantren\BaseFunctions;
    
    // Модули, которые будут подключены
    protected $needModules = ['iblock'];
    
    protected $checkParams = [
        'IBLOCK_CODE' => ['type' => 'string'],
    ];
    
    protected $cacheTemplate = false;
    
    protected function executeMain()
    {
        $this->setParamsFilters();
        $this->setParamsNav();
        
        $request = Application::getInstance()->getContext()->getRequest();
        
        if ($this->isAjax()) {
            //Устанавливаем шаблон страницы
            $this->templatePage = $request->get('tpl') ? $request->get('tpl') : $this->templatePage;
            
            //Устанавливаем страницу для постраничной навигации
            $this->addParamsNavStart(['iNumPage' => $request->get('page') ? $request->get('page') : 1]);
            
            //Устанавливаем параметры портировки и порядка
            $this->arParams['SORT_BY_1'] = $request->get('sort') ? strtoupper($request->get('sort')) : $this->arParams['SORT_BY_1'];
            $this->arParams['SORT_ORDER_1'] = $request->get('order') ? strtoupper($request->get('order')) : $this->arParams['SORT_ORDER_1'];
            
            //Если пришел запрос на добавление - пытаемся добавить закладку и возвращаем json ответ
            if ($request->get('action') == 'add' && $request->get('addUrl')) {
                
                $res = $this->addBookmark($request);
                echo json_encode($res);
                
                exit;
            }
            
            if ($request->get('action') == 'exportExcel') {
                
                $res = $this->exportExcel();
                echo json_encode($res);
                
                exit;
            }
        }
        
        //Получаем список закладок
        $this->arResult['LIST'] = [];
        $rsElements = \CIblockElement::GetList(
            $this->getParamsSort(),
            $this->getParamsFilters(),
            $this->getParamsGrouping(),
            $this->getParamsNavStart(),
            $this->getParamsSelected()
        );
        
        $processingMethod = $this->getProcessingMethod();
        while ($element = $rsElements->$processingMethod(false, false)) {
            if ($arElement = $this->processingElementsResult($element)) {
                $this->arResult['LIST'][] = $arElement;
            }
        }
        
        //Генирируем строку постраничной навигации
        $this->generateNav($rsElements);
    }
    
    public function prepareElementsResult($el)
    {
        $el = $this->prepareElement($el);
        
        $el['BUTTONS'] = $this->getButtons($el['IBLOCK_ID'], $el['ID']);
        
        return $el;
    }
    
    /**
     * Trying to add a bookmark.
     * Return ['status' => true, 'newId' => newId] in case of success
     * or ['status' => false, 'msg' => msg] in case of fail
     *
     * @param $req Reuest
     * @return array
     */
    public function addBookmark($req)
    {
        //Разбираем пришедший урл
        preg_match('/^(http|https):\/\/(.*)/', $req->get('addUrl'), $matches);
        if($matches) {
            $protocol = $matches[1];
            $domain = $matches[2];
        } else {
            return ['status' => false, 'msg' => Loc::getMessage('ERROR_CONNECTION_URL')];
        }
        $Punycode = new Punycode();
        //Составляем новый урл с кодированными русскими символами, если есть
        $url = $protocol . '://' .$Punycode->encode($domain);
        $pass = $req->get('password') ? password_hash($req->get('password'), PASSWORD_BCRYPT) : '';
        
        $hasUrl = CIBlockElement::GetList([],
            [
                'IBLOCK_CODE' => $this->arParams['IBLOCK_CODE'],
                'PROPERTY_URL' => trim($url, '/')
            ], false, false, []
        )->Fetch();
        
        //Если уже есть сайт с таким url - возвращаем соответсвующую ошибку
        if ($hasUrl) {
            return ['status' => false, 'msg' => Loc::getMessage('ERROR_ALREADY_HAS_URL')];
        }
        
        //Если такой сайт недоступен - возвращаем соответсвующую ошибку
        if (!$this->isUrlExist($url)) {
            return ['status' => false, 'msg' => Loc::getMessage('ERROR_CONNECTION_URL')];
        }
        
        //В качестве положительного ответа сервера принимаем все 2хх и 3хх коды ответа, иначе - возвращаем соответсвующую ошибку
        if (!preg_match('/^(2|3)\d{2}$/', $this->getHttpCode($url))) {
            return ['status' => false, 'msg' => Loc::getMessage('ERROR_BAD_HTTP_CODE') . " {$this->getHttpCode($url)}"];
        }
        
        $metaData = $this->getMetaData($url);
        
        $bookmarksIblockId = CIBlock::GetList([], ['IBLOCK_CODE' => $this->arParams['IBLOCK_CODE']], false)->Fetch()['ID'];
        
        $el = new CIBlockElement();
        
        $newId = $el->Add([
            'IBLOCK_ID' => $bookmarksIblockId,
            'PROPERTY_VALUES' =>
                [
                    'URL' => trim($url, '/'),
                    'FAVICON' => $this->getFaviconUrl($url),
                    'TITLE' => $metaData['title'],
                    'KEYWORDS' => $metaData['keywords'],
                    'DESCRIPTION' => $metaData['description'],
                    'PASSWORD' => $pass
                ],
            'NAME' => $metaData['title'] ? $metaData['title'] : parse_url($url, PHP_URL_HOST),
            'ACTIVE' => 'Y',
        ]);
        
        return $newId ?
            ['status' => true, 'newId' => $newId] :
            ['status' => false, 'msg' => Loc::getMessage('ERROR_ADD_BOOKMARK')];
        
    }
    
    /**
     * Get URL Favicon if it's exist
     *
     * @param $url string
     * @return string
     */
    public function getFaviconUrl($url)
    {
        $arFile = CFile::MakeFileArray('http://www.google.com/s2/favicons?domain=' . $url);
        
        return CFile::SaveFile($arFile, 'favicons/' . parse_url($url, PHP_URL_HOST));
    }
    
    /**
     * Get URL Meta data
     *
     * @param $url string
     * @return string
     */
    public function getMetaData($url)
    {
        //Получаем html страницы
        $html = $this->fileGetContentsCurl($url);
        
        if (!$html) {
            return [];
        }
        
        $doc = new DOMDocument();
        $doc = $this->loadHTML($doc, $html, 'utf-8');
        $nodes = $doc->getElementsByTagName('title');
        
        //Получаем тайтл страницы
        $result['title'] = $nodes->item(0)->nodeValue;
        
        //Получение мета данных страницы
        $metas = $doc->getElementsByTagName('meta');
        
        for ($i = 0; $i < $metas->length; $i++) {
            $meta = $metas->item($i);
            if ($meta->getAttribute('name') == 'description')
                $result['description'] = $meta->getAttribute('content');
            if ($meta->getAttribute('name') == 'keywords')
                $result['keywords'] = $meta->getAttribute('content');
        }
        
        return $result;
    }
    
    /**
     * Check if URL is exist
     *
     * @param $url string
     * @return bool
     */
    public function isUrlExist($url)
    {
        $parseUrl = parse_url($url);
        if (empty($parseUrl['host']) || !@get_headers($url)) {
            return false;
        }
        return true;
    }
    
    /**
     * Rerurn HTTP code of connection to domain
     *
     * @param $url string
     * @return bool
     */
    public function getHttpCode($url)
    {
        $headers = @get_headers($url);
        if (!empty($headers[0])) {
            preg_match('/\d{3}/', $headers[0], $matches);
            return $matches[0];
        } else {
            return false;
        }
    }
    
    public function loadHTML($dom, $source, $encoding) {
        $source = '<meta http-equiv="Content-Type" content="text/html; charset='.
            $encoding.'">' . $source;
        $dom->loadHTML($source);
        
        return $dom;
    }
    
    public function exportExcel()
    {
        //Получаем список всех закладок
        $rsElements = \CIblockElement::GetList(
            $this->getParamsSort(),
            $this->getParamsFilters(),
            false,
            false,
            $this->getParamsSelected()
        );
        
        $processingMethod = $this->getProcessingMethod();
        while ($element = $rsElements->$processingMethod(false, false)) {
            if ($arElement = $this->processingElementsResult($element)) {
                $bookmarks[] = $arElement;
            }
        }
        
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        
        //Заголовная строка таблицы
        $sheetHeader = [
            Loc::getMessage('POSITION_NUMBER'),
            Loc::getMessage('DATE_ADD'),
            Loc::getMessage('PAGE_URL'),
            Loc::getMessage('PAGE_TITLE'),
            Loc::getMessage('META_DESCRIPTION'),
            Loc::getMessage('META_KEYWORDS'),
            Loc::getMessage('FAVICON'),
        ];
        
        foreach ($sheetHeader as $key => $cellText) {
            $sheet->setCellValueByColumnAndRow($key + 1, 1, $cellText);
        }
        
        //Записываем закладки в таблицу
        foreach ($bookmarks as $key => $bookmark) {
            $sheet->setCellValueByColumnAndRow(1, $key + 2, $key + 1);
            $sheet->setCellValueByColumnAndRow(2, $key + 2, $bookmark['DATE_CREATE']);
            $sheet->setCellValueByColumnAndRow(3, $key + 2, $bookmark['PROPS']['URL']['VALUE']);
            $sheet->setCellValueByColumnAndRow(4, $key + 2, $bookmark['PROPS']['TITLE']['VALUE']);
            $sheet->setCellValueByColumnAndRow(5, $key + 2, $bookmark['PROPS']['DESCRIPTION']['VALUE']);
            $sheet->setCellValueByColumnAndRow(6, $key + 2, $bookmark['PROPS']['KEYWORDS']['VALUE']);
            $sheet->setCellValueByColumnAndRow(7, $key + 2, $this->getSiteUrl() . $bookmark['PROPS']['FAVICON']['SRC']);
        }
    
        $sheet->calculateColumnWidths();
        
        $writer = new Xlsx($spreadsheet);
        
        $fileDir = '/upload/excel/';
        $fileName = 'bookmarks.xlsx';
        
        $fileRealPath = $this->makeDirIfNotExist($fileDir) . $fileName;

        $writer->save($fileRealPath);
        
        return ['status' => true, 'filePath' => $this->getSiteUrl() . $fileDir . $fileName];
    }
    
    /**
     * Go through dir path and make all directories
     *
     * @param $path string
     * @return string
     */
    public function makeDirIfNotExist($path)
    {
        $pathArr = explode('/', trim(str_replace($_SERVER['DOCUMENT_ROOT'], '', $path), '/'));
        $resPath = $_SERVER['DOCUMENT_ROOT'];
        foreach ($pathArr as $dirName) {
            $resPath .= '/' . $dirName;
            if (!is_dir($resPath)) {
                mkdir($resPath);
            }
        }
        
        return $resPath . '/';
    }
    
}
