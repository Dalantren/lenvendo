<?php require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');?>
    <div class="wrap">
        <?php $APPLICATION->IncludeComponent(
            'dalantren:bookmarks.detail','single',
            [
                'IBLOCK_CODE'               => 'bookmarks',
                'RESULT_PROCESSING_MODE'    => 'EXTENDED',
                'ELEMENTS_COUNT'            => 5,
                'SELECT_FIELDS'             => [
                    'DATE_CREATE',
                    'LIST_PAGE_URL'
                ],
                
                'USE_AJAX'  => 'Y',
                'AJAX_COMPONENT_ID'         => 'bookmark',
                'AJAX_PARAM_NAME'           => 'compid',
                
            ], false, ['HIDE_ICONS' => 'Y']);
        ?>
    </div>
<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');?>