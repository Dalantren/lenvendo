<?php require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');?>
<div class="wrap">
<?php $APPLICATION->IncludeComponent(
	'dalantren:bookmarks.list','list',
	[
        'IBLOCK_CODE'               => 'bookmarks',
        'SORT_BY_1'                 => 'SORT',
        'ORDER_BY_1'                => 'ASC',
        'RESULT_PROCESSING_MODE'    => 'EXTENDED',
        'ELEMENTS_COUNT'            => 5,
        'SELECT_FIELDS'             => [
                                        'DATE_CREATE',
                                        'DETAIL_PAGE_URL'
                                    ],

        'USE_AJAX'                  => 'Y',
        'AJAX_COMPONENT_ID'         => 'bookmarks',
        'AJAX_PARAM_NAME'           => 'compid',
        
		'DISPLAY_BOTTOM_PAGER'      => 'Y',
		'PAGER_SAVE_SESSION'        => 'Y',
		'PAGER_TEMPLATE'            => 'pager',		
    ], false, ['HIDE_ICONS' => 'Y']);
?>
</div>
<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');?>